package com.springapp.mvc;

import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by darshana on 2017-01-21.
 */
@Service
public class UserService {

    private static Map<Integer,User> userMap = new HashMap<Integer,User>();

    static {
        User u1 = new User(1,"Darshana","Welikala","Canada","Software Engineer");
        User u2 = new User(2,"Sampath","Welikala","Mississagua","Software Engineer");
        User u3 = new User(3,"Darshana","Wijegunarathna","Toronto","Front End Developer");

        userMap.put(u1.getId(),u1);
        userMap.put(u2.getId(),u2);
        userMap.put(u3.getId(),u3);
    }

    public List getUsers(){
        List<User> arrs = new ArrayList<User>();
        arrs.addAll(userMap.values());
        return arrs;
    }

    public void createUser(User user){
        userMap.put(userMap.size()+1,user);
    }

    public void updateUser(User user){
        userMap.remove(user.getId());
        userMap.put(user.getId(),user);
    }

    public void deleteUser(Integer id){
        userMap.remove(id);
    }

    public User findUserById(Integer id){
        return userMap.get(id);
    }

    public boolean isUserExist(User user){
        return false;
    }

}
